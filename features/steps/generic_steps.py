from behave import *

@given(u'que quiero probar algo')
def step_impl(context):
  raise NotImplementedError(u'STEP: Given que quiero probar algo')


@when(u'realizo "X"')
def step_impl(context):
  raise NotImplementedError(u'STEP: When realizo "X"')


@then(u'valido el resultado de "X"')
def step_impl(context):
  raise NotImplementedError(u'STEP: Then valido el resultado de "X"')